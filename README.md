# Cube Mechanical
This repo contains the 3d models of the custom parts required to build the Cube, a CoreXY motion platform for moving sensors.
It also contains the BOM with all the required parts.

The parts for 3D printing are in the `/stl` directory.

All parts are licensed under CERN OHL-W, full license in `LICENSE.md`.

![The Cube](cube.png)