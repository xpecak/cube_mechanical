## 3D printed parts

|Part|Quantity|Notes|
|:---|:-----|:----|
|`X_carriage_body.stl`| 1 | X carriage body |
|`X_carriage_clamp.stl`| 2 | X carriage belt clamps |
|`X_carriage_cover.stl`| 2 | X carriage bearing cover |
|`XY_home_switch_1.stl`| 1 | XY home switch, Y homing part |
|`XY_home_switch_2.stl`| 1 | XY home switch, Z homing part |
|`XY_motor_L.stl`| 1 | Left XY motor mount |
|`XY_motor_R.stl`| 1 | Mirrored version XY_motor_L |
|`XY_rear_pulley.stl`| 2 | Does not need to be mirrored |
|`Y_carriage_body.stl`| 2 | Y carriage body, both sides are identical, just flipped over |
|`Y_carriage_cover.stl`| 2 | Y Carriage bearing cover |
|`Y_rail.stl`| 4 | Y rails holder, two can be printed mirrored for easier fit |
|`Z_carriage.stl`| 2 | Z axis bearing holder |
|`Z_home_switch.stl`| 1 | Z axis homing switch holder |
|`Z_leadscrew_nut.stl`| 2 | Z axis leadscrew nut mount |
|`Z_motor.stl`| 2 | Z axis motor holder, each side is same |
|`Z_rail.stl`| 8 | Z rails holder |
|`foot.stl`| 4 | for protecting the table |
|`sensor_mount.stl`| 1 | optional sensor mount for MLX90393 devkit, mounts onto X carriage |
|`board_mount.stl`| 1 | optional holder for the Cube Board shield |
|`cable_hook.stl`| 1 | optional, mounts on top of X carriage |
|`logo.stl`| 1 | optional, cosmetic logo|

## Structural parts
Note: The cube is designed with the 6mm slot european style extrusions.

|Part|Quantity|Notes|
|:---|:-----|:----|
| 2020 aluminium extrusion 500mm | 4 |  |
| 2020 aluminium extrusion 350mm | 10 |  |
| 2020 aluminium extrusion 270mm | 4 |  |
| 2020 extrusion corner bracket | 32 |  |

## Motion related parts

|Part|Quantity|Notes|
|:---|:-----|:----|
| 360mm, 8mm dia precision steel shaft | 2 | X-axis rails |
| 350mm, 8mm dia precision steel shaft | 2 | Y-axis rails |
| 315mm, 8mm dia precision steel shaft | 4 | Z-axis rails |
| LM8LUU bearings | 4 | XY bearings |
| LMH8UU bearings with flange | 4 | Z-axis bearings |
| ~5m, GT2 timing belt, 6mm wide | 1 | 5m should be enough for the build |
| GT2 idler pulley, 16T dia, 3mm hole, with teeth | 6 |  |
| GT2 idler pulley, 16T dia, 3mm hole, without teeth(smooth) | 2 | can be replaced by toothed pulley |
| GT2 Pulley, 16T, for 5mm shaft | 2 | Stepper pulleys |
| NEMA17 motor | 2 | original build uses `17HS3401` motor |
| NEMA17, 300mm integrated T8 leadscrew | 2 | original build uses `17HS3401` |

## Screws, nuts

All screws are cylindrical head hex socket.

|Part|Quantity|
|:---|:-----|
| 2020 profile T-nut M4 | 140 |
| M3 square nut | 40 |
| M3 nut | 20 |
| M3 nylock nut | 6 |
| M4 8mm | 136 |
| M4 12mm | 4 |
| M3 8mm | 50 |
| M3 16mm | 26 |
| M3 30mm | 6 |
| M3 20mm | 4 |
| M3 10mm | 4 |


Optional parts:

|Part|Quantity|Notes|
|:---|:-----|:----|
| 2020 profile T-nut M4 | 2 | board holder |
| M4 12mm | 2 | board holder |
| M3 20mm standoff | 4 | board holder |
| M3 5mm standoff | 4 | sensor holder, nylon if possible |
| M3 6mm | 8 | 4x board holder, 4x sensor holder(nylon if possible) |
| M3 square nut | 4 | board holder |
